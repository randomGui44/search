#include <iostream>
#include <string>

using namespace std;

int search(string text, string pattern){	
	
	int initial_index;
	
	if(text.size() == 0){		
		return -1;
	}//end If
	else{
		for(int count = 0; count <= text.size()-1; count++){
			
			bool index_found = true;
			for(int count2 = 0; count2 <= pattern.size() -1; count2++){
				if(pattern[count2] != text[count+count2])
				{
					index_found = false;
					initial_index = 0;
					break;
				}//End if
			}//End For
			if(index_found){
				return count;
			}//End If
		}//End For
		return -1;
	}//End else
	
}//end Search
